Booking
*******

Django application for booking

Install
=======

Virtual Environment
-------------------

::

  virtualenv --python=python3 venv-booking
  # or
  python3 -m venv venv-booking

  source venv-booking/bin/activate

  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  pytest -x

Usage
=====

::

  ./init_dev.sh

Release
=======

https://www.kbsoftware.co.uk/docs/
